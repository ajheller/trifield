/*
This file is part of the Ambisonic Decoder Toolbox (ADT).
Copyright (C) 2015  Aaron J. Heller 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
    
You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
declare name		"ADT Shelf";
declare version 	"1.0";
declare author 		"Aaron J. Heller <heller@ai.sri.com>";
declare license 	"GPL";
declare copyright	"(c) Aaron J. Heller 2013";
*/

//  libraries have been reorganized
// math = library("math.lib");

math = environment {
  SR = min(192000, max(1, fconstant(int fSamplingFreq, <math.h>)));
  // quad precision value
  PI = 3.14159265358979323846264338327950288;
};


// physical constants     

temp_celcius = 20;                        
c = 331.3 * sqrt(1.0 + (temp_celcius/273.15)); // speed of sound m/s

// gain bus
gain(c) = R(c) with {
  R((c,cl)) = R(c),R(cl);
  R(1)      = _;
  R(0)      = !;
  R(float(0)) = R(0);
  R(float(1)) = R(1);
  R(c)      = *(c);
};

// fir filter
fir(c) = R(c) :>_ with {
  R((c,lc)) = _<: R(c), (mem:R(lc));
  R(c)      = gain(c);
};

// phase-matched bandsplitter from BLaH3
// 
// The key criteria is that the outputs sum to an allpass filter.
//  lf(s) = 1/(1+2s+s^2)
//  hf(s) = s^2/(1+2s+s^2)
// so,
//  lf - hf = (1-s)/(1+s), which is a first-order allpass
//
//  see Appendix 2 of BLaH3 for further details
//  A. Heller, R. Lee, and E. M. Benjamin, "Is My Decoder Ambisonic?"
//  presented at the 125th AES Convention, San Francisco, 2008, pp. 1–21.

xover(freq,n) = par(i,n,xover1) with {

	sub(x,y) = y-x;

	k = tan(math.PI*float(freq)/math.SR);
	k2 = k^2;
	d =  (k2 + 2*k + 1);
	//d = (k2,2*k,1):>_;
	// hf numerator
	b_hf = (1/d,-2/d,1/d);
	// lf numerator
	b_lf = (k2/d, 2*k2/d, k2/d);
	// denominator
	a = (2 * (k2-1) / d, (k2 - 2*k + 1) / d);
	//
	xover1 = _:sub ~ fir(a) <: fir(b_lf),fir(b_hf):_,*(-1);
};

shelf(freq,g_lf,g_hf) = xover(freq,1) : gain(g_lf),gain(g_hf) :> _;

// testing
//  process = shelf(400,2,0.5);

// -------------------------------------------------
// Trifield decoder based on Figure 16 of [1]
// 
// [1] M. A. Gerzon, "Optimum Reproduction Matrices for Multispeaker Stereo," J. Audio Eng Soc, 
//     vol. 40, no. 7, p. 571, 1992.
// [2] (need citation)
//  http://www.diyaudio.com/forums/multi-way/222881-trinaural-decoding-equations-3-speaker-stereo-matrix-2.html
//
// more discussion:
//  http://elias.altervista.org/html/3_speaker_matrix.html
//  http://elias.altervista.org/html/2_vs_3_stereo_high_freq.html
//  http://www.stereophile.com/content/upward-mobility-2-channels-surround-page-2
//  http://www.diyaudio.com/forums/multi-way/116300-linear-2-3-rematrixing-speaker-signals.html
//  http://www.diyaudio.com/forums/multi-way/193973-making-3-front-channels-out-stereo-signal-7.html
//  http://redelectron.eu/trifield-decoder/


declare name	  "Trifield LR->LRC decoder";
declare version	  "1.0";
declare author	  "AmbisonicDecoderToolkit";
declare license   "GPL";
declare copyright "(c) Aaron J. Heller 2013, Note: Trifield is a trademark of Trifield Audio Ltd.";


// UI "dezipper"
smooth(c) = *(1-c) : +~*(c);
dezipper = smooth(0.999);

// ms conversion (works either way)
ms_matrix = _,_: /(sqrt(2)),/(sqrt(2)) <: +,- : _,_;

// In Gerzon's paper the most frequently mentioned configuration 
// for three speakers is 90 deg from left to right.
// He says phi_lf = 35deg, phi_hf = 55 deg, seems optimal.  

// phi_lf is actually atan2(1, sqrt(2)) * 180/pi = ~35.2644 deg
// phi_hf atan2(sqrt(2),1) * 180/pi = ~54.7356 deg

phi_lf = (math.PI/180) * hslider("phi_lf [unit:deg]",  35.26, 0, 90, 0.5) : dezipper ;  // degrees
phi_hf = (math.PI/180) * hslider("phi_hf [unit:deg]",  55.70, 0, 90, 0.5) : dezipper ;  // degrees

cos_sin_panpot(phi) = _<: gain(cos(phi)), gain(sin(phi)) : _,_ ;

// band splitting frequency
xover_freq = hslider("xover [unit:Hz][tooltip:Crossover frequency (Hz)]", 3000, 1000, 10000, 100);  //Hz

// side width control
side_width_control(x) = x * c
  with {
    c = hslider("width [unit:gain][tooltip:Stereo Width]", 1.0, 0, 2, 0.01) : dezipper ;
  };
// we use a crossover with outputs summed to make the allpass filter needed to match the phase of the S signal
side_width = xover(xover_freq,1):>_: side_width_control;

c_delay = @(meters2samples(hslider("c_dx [unit:meters][tooltip:Center speaker delay (meters)", 0, 0, 1.0, 0.005))) with {
	meters2samples(r) = int(math.SR * (float(r)/float(c)) + 0.5);
};

clr2lrc(c,l,r) = l,r,c;

roll = _,_,_ <: (_,_,_: !,_,! :>_) ,
                (_,_,_: !,!,_ :>_) , 
                (_,_,_: _,!,! :>_);

lf_cos_sin = cos_sin_panpot(phi_lf);
hf_cos_sin = cos_sin_panpot(phi_hf);

allpass = xover(xover_freq,1) :>_;
bandsplit = xover(xover_freq,1) :_,_;

trifield4 = _,_ : ms_matrix : 
                    (_: bandsplit : lf_cos_sin, hf_cos_sin :> _,_),
                    (_: allpass : side_width_control) :
		  (c_delay, ms_matrix) : clr2lrc ;

process = trifield4;
